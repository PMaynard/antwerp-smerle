# Antwerp Smerle

The one true portable media player.

Antwerp Smerle, is a pigeon. This project is about creating a MP3 player/Podcatcher/Internet Streaming device using postmarketOS and a RaspberryPi Zero.

# Todo

- [ ] Local Media (mpd)
	- [x] Over HTTP 
	- [ ] Bluetooth Speakers
- [ ] Control Media via Bluetooth
- [ ] Internet Radio
- [ ] Podcasts

# Issues

- There are some CPU 100% problems

# Notes

	.local/bin/pmbootstrap install --sdcard /dev/sdb --no-fde

	sudo rc-update add mpd default
	mkdir -p /data/music /data/playlists /data/.tmp/mpd/tmp 
	sudo chown -R user:nogroup /data
	sudo chown -R mpd:nogroup mpd/
	logread -f 
	rc-status 

